package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class BaseOfCreation {



    public static Question createQuestion() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите вопрос");
        String questionText = scanner.nextLine();

        System.out.println("Введите количество ответов");
        int answerCount = scanner.nextInt();

        System.out.println("Введите стоимость вопроса");
        int value = scanner.nextInt();
        scanner.nextLine();

        ArrayList<Answer> answersList = new ArrayList<Answer>();

        boolean isRight = false;
        for (int i = 0; i < answerCount; i++) {
            System.out.println("Введите вариант ответа");
            String text = scanner.nextLine();

            System.out.println("Этот ответ является верным?");
            String yesOrNo = scanner.nextLine();
            switch (yesOrNo) {
                case ("да"):
                    isRight = true;
                    break;
                case ("нет"): {
                    isRight = false;
                    break;
                }
                default:
                    System.out.println("Ответ не распознан.Введите 'да' или 'нет' ");
            }

            Answer answer = new Answer(text,isRight);
            answersList.add(answer);
        }
        Question question = new Question(questionText, value, answersList);
        return question;
    }
}
