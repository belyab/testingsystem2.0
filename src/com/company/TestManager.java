package com.company;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class TestManager {

    private static ArrayList<Question> database = new ArrayList<Question>();

    public void init() {

        System.out.println("Добро пожаловать в систему тестирования.");


        ArrayList<Question> db = addQuestions(database);

        Scanner scanner = new Scanner(System.in);



            System.out.println("Введите вашу должность (ученик/преподаватель)");
            String position = scanner.nextLine();

            switch (position) {
                case ("ученик"): {
                    System.out.println("На данный момент в системе " + database.size() + " вопросов");
                    startTest(db);
                    break;
                }
                case ("преподаватель"): {
                    addQuestions(db);
                    break;
                }
            }
        }



    private ArrayList<Question> addQuestions(ArrayList<Question> database) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество вопросов,которе вы хотите добавить");

        int num = scanner.nextInt();

        for (int i = 0; i < num; i++) {
            Question newQuestion = BaseOfCreation.createQuestion();
            database.add(newQuestion);
        }
        return database;
    }

    private ArrayList<Question> startTest(ArrayList<Question> database) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите число вопросов,которые будут в тесте");
        int num = scanner.nextInt();

        while (num - 1 >= database.size()) {
            System.out.println("Количество вопросов не может быть больше чем " + database.size() + " .Введите заново");
            num = scanner.nextInt();
        }

            //здесь вопросики для определенного теста
            ArrayList<Question> newList = new ArrayList<Question>();

            int count = num;

            // здесь временно хранятся рандомные вопросы
            ArrayList<Question> tmp_database = database;
            for (int i = 0; i < num; i++) {
                int randomNum = ThreadLocalRandom.current().nextInt(0, count);

                Question certainQuestion = tmp_database.get(randomNum);
                newList.add(certainQuestion);

                tmp_database.remove(randomNum);
                count--;
            }


            int valueScore = 0;

            //хранение вопросв,на которые ответ неверный
            ArrayList<Question> wrongQuestions = new ArrayList<Question>();

            for (int i = 0; i < newList.size(); i++) {
                System.out.println(newList.get(i).questionText);

                int answerLength = newList.get(i).answers.size();
                for (Integer j = 0; j < answerLength; j++) {
                    String textAnswer = j.toString() + ' ' + newList.get(i).answers.get(j).answerText;
                    System.out.println(textAnswer);
                }
                System.out.println("Введите номер ответа");

                int answerNumber = scanner.nextInt();
                while (answerNumber >= answerLength || answerNumber < 0) {
                    System.out.println("Введите один из номеров ответа:");
                    answerNumber = scanner.nextInt();
                }

                if (newList.get(i).answers.get(answerNumber).isRight) {
                    valueScore += newList.get(i).value;
                } else wrongQuestions.add(newList.get(i));
            }
            System.out.println("Тестирование окончено.Набранное количество баллов: " + valueScore);
            System.out.println("Неверные ответы : ");

            for (int i = 0; i < wrongQuestions.size(); i++) {
                System.out.println(wrongQuestions.get(i).questionText);
                for (int j = 0; j < wrongQuestions.get(i).answers.size(); j++) {
                    if (wrongQuestions.get(i).answers.get(j).isRight) {
                        System.out.println("Правильный ответ: " + wrongQuestions.get(i).answers.get(j).answerText);
                    }
                }
            }
        return database;
    }

    private ArrayList<Question> start(){
        ArrayList<Question> db = addQuestions(database);

        return startTest(db);


    }
}
