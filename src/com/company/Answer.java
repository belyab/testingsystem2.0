package com.company;

public class Answer {
    public boolean isRight;
    public String answerText;

    public Answer(String answerText,boolean isRight){
        this.answerText=answerText;
        this.isRight=isRight;
    }
}
